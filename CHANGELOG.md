# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.21"></a>
## [0.1.21](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.20...v0.1.21) (2018-12-21)
- VRx status lights color inverse


<a name="0.1.20"></a>
## [0.1.20](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.19...v0.1.20) (2018-12-21)
- VRx IP Address update when it will appear again


<a name="0.1.19"></a>
## [0.1.19](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.18...v0.1.19) (2018-12-20)
- Current local IP address detection logs


<a name="0.1.18"></a>
## [0.1.18](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.17...v0.1.18) (2018-12-20)



<a name="0.1.17"></a>
## [0.1.17](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.16...v0.1.17) (2018-12-17)



<a name="0.1.16"></a>
## [0.1.16](https://gitlab.com/medivrx/MediVRx-MediaServer/compare/v0.1.15...v0.1.16) (2018-12-14)



<a name="0.1.15"></a>
## 0.1.15 (2018-12-04)
